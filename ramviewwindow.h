#ifndef RAMVIEWWINDOW_H
#define RAMVIEWWINDOW_H

#include <QtWidgets>
#include <QtQml>

class RamViewWindow : public QMainWindow
{
        Q_OBJECT
    public:
        explicit RamViewWindow(QWidget *parent = 0);
        void setupScene();
        void setupVisualScripting();
        void setupAudioUpdater();
        void resetAudioUpdater();
        void startVisuals();
        void stopVisuals();
        void loadScript(const QString& path);
        std::vector<QGraphicsEllipseItem*> chrect;
        std::vector<QGraphicsEllipseItem*> chrect2;
        QWidget* viewport;
        QFileSystemWatcher watcher;
        QJSEngine scriptEngine;
        std::vector<QJSValue> scriptNoteValues;
        std::vector<QJSValue> scriptVisualValues;
        QString visualScript;
        QJSValue visualUpdateValue;
        QTimer* displayTimer;

        std::vector<QGraphicsSimpleTextItem*> chtext;
        //std::vector<NutMixerNote*> mixerNotes;
        //std::vector<NutVisual*> visuals;

    signals:
        
    public slots:
        void scriptChanged(const QString &path);
        void doUpdate();

    private:
        QGraphicsScene scene;
        QGraphicsView view;

};

#endif // RAMVIEWWINDOW_H
