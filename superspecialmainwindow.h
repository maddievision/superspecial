#ifndef SUPERSPECIALMAINWINDOW_H
#define SUPERSPECIALMAINWINDOW_H

#include <QtWidgets>
class SuperSpecialCore;
class SuperSpecialMainWindow : public QMainWindow
{
        Q_OBJECT
    public:
        explicit SuperSpecialMainWindow(QWidget *parent = 0);
        void setupMenu();
        void setupTabs();
        SuperSpecialCore* core;
        QString filename;
        QString lastPath;
        QMenu* fileMenu;
        QMenu* optionsMenu;
        QMenu* controlMenu;
        QAction* fileOpenAction;
        QAction* controlPlayFromStartAction;
        QAction* controlResumePlaybackAction;
        QAction* controlStopAction;
        QVBoxLayout* layout;
        QTabWidget* tabWidget;

    signals:
        
    public slots:
        void openFile(QString fname);
        void fileOpen();
        void playFromStart();
        void resumePlayback();
        void stopPlayback();

};

#endif // SUPERSPECIALMAINWINDOW_H
