#ifndef GME_TYPES_H
#define GME_TYPES_H

/* CMake will either define the following to 1, or #undef it,
 * depending on the options passed to CMake.  This is used to
 * conditionally compile in the various emulator types.
 *
 * See gme_type_list() in gme.cpp
 */

/* #undef USE_GME_AY */
/* #undef USE_GME_GBS */
/* #undef USE_GME_GYM */
/* #undef USE_GME_HES */
/* #undef USE_GME_KSS */
/* #undef USE_GME_NSF */
/* #undef USE_GME_NSFE */
/* #undef USE_GME_SAP */
/* #undef USE_GME_SPC */
/* VGM and VGZ are a package deal */
/* #undef USE_GME_VGM */

#endif /* GME_TYPES_H */
