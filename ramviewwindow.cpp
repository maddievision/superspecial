#include "ramviewwindow.h"

RamViewWindow::RamViewWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->setupScene();
    this->setupVisualScripting();
    this->setupAudioUpdater();
}


void RamViewWindow::setupScene() {
    scene.setSceneRect(QRectF(0,0,1024,800));
    scene.setBackgroundBrush(Qt::black);
    viewport = view.viewport();
    view.setAlignment(Qt::AlignTop | Qt::AlignLeft);
    view.setScene(&scene);
    view.setRenderHint(QPainter::Antialiasing,true);
    this->setCentralWidget(&view);
    QFile res(":/font/04b03.ttf");
    int fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
    QGraphicsSimpleTextItem* si = scene.addSimpleText("Dickington Palace!",QFont("04b03", 16));
    si->setBrush(Qt::white);
    si->setPos(20,20);

}

void RamViewWindow::scriptChanged(const QString &path) {
    loadScript(path);
}

void RamViewWindow::loadScript(const QString &path) {
    //i don't want no STUB
}

void RamViewWindow::setupVisualScripting() {
    QString scriptPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + QDir::separator() + "spcp.js";
    loadScript(scriptPath);
//    connect(&this->watcher,SIGNAL(fileChanged(QString)),this,SLOT(scriptChanged(QString)));
//    this->watcher.addPath(scriptPath);
}

void RamViewWindow::resetAudioUpdater() {
    //i don't want no STUB
}

void RamViewWindow::startVisuals() {
    this->displayTimer->start(20);
}
void RamViewWindow::stopVisuals() {
    this->displayTimer->stop();
}

void RamViewWindow::setupAudioUpdater() {
    displayTimer = new QTimer(this);
    connect(displayTimer, SIGNAL(timeout()), this, SLOT(doUpdate()));
}


void RamViewWindow::doUpdate() {
    viewport->update();
}
