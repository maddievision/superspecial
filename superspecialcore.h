#ifndef SUPERSPECIALCORE_H
#define SUPERSPECIALCORE_H

#include <QtWidgets>
#include <QtQml>

#include "Spc_Emu.h"
#include "portaudio.h"
typedef struct
{
    Music_Emu* emu;
    int counter;
} paTestData;

class SuperSpecialCore : public QObject
{
        Q_OBJECT
    public:
        explicit SuperSpecialCore(QObject *parent = 0);
        Spc_Emu*  spc;
        bool mutedchannel[8];
        int solochannel;
        bool soloing;
        bool playing;
        PaStream *stream;
       paTestData data;
        void updatemute();
        void ToggleMute(int channel);
        void ToggleSolo(int channel);
        void ResetVol();
        void openFile(QString filename);
        void stop();
        void playFromStart();
        void resume();
    signals:
        
    public slots:
        
};

#endif // SUPERSPECIALCORE_H
