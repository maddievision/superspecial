#include "superspecialcore.h"
#include <stdint.h>
#include "gme.h"
#include "portaudio.h"
#include <stdio.h>
#include <vector>
#include <string>
#include <map>

#define SAMPLE_RATE (32000)
#define FRAME_RATE (paFramesPerBufferUnspecified)

static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData );

SuperSpecialCore::SuperSpecialCore(QObject *parent) :
    QObject(parent), spc(NULL)
{
    PaError err = Pa_Initialize();
  //	if( err != paNoError ) goto error;

    printf("Opening stream...\n");
    /* Open an audio I/O stream. */
    err = Pa_OpenDefaultStream( &stream,
                                0,          /* no input channels */
                                2,          /* stereo output */
                                paInt16,  /* 32 bit floating point output */
                                SAMPLE_RATE,
                                FRAME_RATE,        /* frames per buffer, i.e. the number
                                                   of sample frames that PortAudio will
                                                   request from the callback. Many apps
                                                   may want to use
                                                   paFramesPerBufferUnspecified, which
                                                   tells PortAudio to pick the best,
                                                   possibly changing, buffer size.*/
                                patestCallback, /* this is your callback function */
                                &data ); /*This is a pointer that will be passed to
                                                   your callback*/
  //  if( err != paNoError ) goto error;

}

void SuperSpecialCore::updatemute() {
  int mask = 0;
  for (int i = 0; i < 8; i++) {
    if (mutedchannel[i]) mask += 1 << i;
  }
  spc->apu.dsp.mute_voices(mask);
}
void SuperSpecialCore::ToggleMute(int channel) {
  mutedchannel[channel] = !mutedchannel[channel];
  updatemute();

}
void SuperSpecialCore::ToggleSolo(int channel) {
  mutedchannel[channel] = false;
  solochannel = channel;
  soloing = true;
  for (int i = 0; i < 8; i++) {
    if (channel != i) {mutedchannel[i] = true;
    }
  }
  updatemute();
}
void SuperSpecialCore::ResetVol() {
  for (int i = 0; i < 8; i++) {
    mutedchannel[i] = false;
  }
  soloing = false;
  updatemute();
}

/* This routine will be called by the PortAudio engine when audio is needed.
   It may called at interrupt level on some machines so don't do anything
   that could mess up the system like calling malloc() or free().
*/
static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
    /* Cast data passed through stream to our structure. */
    paTestData *data = (paTestData*)userData;
//    printf("\nCALLEY BACK!!\n %d", data->counter++);
    short *out = (short*)outputBuffer;
    (void) inputBuffer; /* Prevent unused variable warning. */
         gme_play(data->emu, framesPerBuffer * 2, out);
    return 0;
}


const char notes[12][3] = {
  "C ","C#","D ","D#","E ","F ","F#",
  "G ","G#","A ","A#","B "
};

/*
struct game_engine {
  char name[64];
  char prefix[64];
  int addroff;
  int noteoff;
  int noteskip;
  int notetr;
  int ttnaddr;
  int ttnskip;
  int sectaddr;
  int sectskip;
  int instaddr;
  int instskip;
  int octaddr;
  int octskip;
};

void reset_gameengine(game_engine* g) {
  memset(g, 0, sizeof(game_engine));
  g->addroff = -1;
  g->noteoff = -1;
  g->ttnaddr = -1;
  g->sectaddr = -1;
  g->instaddr = -1;
  g->octaddr = -1;
}

struct Watcher {
  std::string id;
  uint32_t base;
  uint32_t base_hi;
  size_t size;
  size_t skip;
  std::map<std::string, int> props;
};

struct GameEngine {
  std::string prefix;
  std::string name;
  std::vector<Watcher> watchers;
};

std::vector<GameEngine> game_engines;

bool load_game_engines() {
  game_engines.clear();
  char homedir[256];
  strcpy(homedir, getenv("HOME"));
  char gepath[256];
  sprintf(gepath, "%s/games.yml",homedir);
  printf("From %s\n",gepath);
  YAML::Node gey = YAML::LoadFile(gepath);
  printf("Parsing...\n");
  for (size_t i=0;i<gey.size();i++) {
    GameEngine g;
    YAML::Node ge = gey[i];
    g.prefix = ge["prefix"].as<std::string>();
    g.name = ge["name"].as<std::string>();
    for (size_t j=0;j<ge["watchers"].size();j++) {
      Watcher w;
      YAML::Node wa = ge["watchers"][j];
      w.id = wa["id"].as<std::string>();
      w.base = wa["base"].as<uint32_t>();
      if (wa["base_hi"]) w.base_hi = wa["base_hi"].as<uint32_t>();
      else w.base_hi = 0xFFFFFFFF;
      w.size = wa["size"].as<size_t>();
      w.skip = wa["skip"].as<size_t>();

      if (wa["prop"]) {
        YAML::Node pr = wa["prop"];
        for(YAML::const_iterator it=pr.begin();it!=pr.end();++it) {
          w.props[it->first.as<std::string>()] = it->second.as<int>();
        }
      }
      g.watchers.push_back(w);
    }
    game_engines.push_back(g);
  }
  return true;
}
*/

void SuperSpecialCore::openFile(QString filename) {
    if (playing) {
        Pa_StopStream(stream);
    }
    if (spc) {
        gme_delete((Music_Emu*) spc);
        spc = NULL;
    }
    gme_open_file(filename.toUtf8(), &data.emu, SAMPLE_RATE);
    gme_enable_accuracy(data.emu,1);
    data.counter= 0;
    spc = (Spc_Emu*)data.emu;
}

void SuperSpecialCore::stop() {
    if (playing) {
    Pa_StopStream(stream);
    playing = false;
    }
}

void SuperSpecialCore::playFromStart() {
    if (playing) Pa_StopStream( stream );
    gme_start_track(data.emu, 0);

    printf("Starting stream...\n");
    Pa_StartStream( stream );
    playing = true;
//	if( err != paNoError ) goto error;
    /* Sleep for several seconds. */
}

void SuperSpecialCore::resume() {
    if (!playing) {
        Pa_StartStream ( stream);
        playing = true;
    }

}

/*


int old_main(int argc, char** argv) {
  printf("Loading Game Engines...\n");
  load_game_engines();
  printf("Complete!\n");

 printf("Opening %s\n",argv[1]);

 printf("Detecting game engine...");
   int gn = -1;

 for (int i = 0; i < game_engines.size(); i++) {
   if (strncmp(argv[1], game_engines[i].prefix.c_str(), game_engines[i].prefix.size()) == 0) {
     printf("%s\n",game_engines[i].name.c_str());
     gn = i;
     break;
   }
 }

  int off = 0;

  int range = 0x10;
  int rows = 0x20;
  int skip = rows * range / 2;

  int marker = 0x0;
  int seladdr = 0x0;

  double emurate = 1.0;

  enum {
    STATE_PLAYING,
    STATE_PAUSED
  } state = STATE_PLAYING;

  state = STATE_PLAYING;


  TClearScreen();
    nonblock(true);

  struct {
    uint16_t addr[8];
    uint8_t notes[8];
  } channels;


  GameEngine* g;

  while (1) {
//    printf("\nSEC %d\n",i++);
//    Pa_Sleep(1000);
    if (gn >= game_engines.size()) gn = -1;
    if (gn >= 0 )
    g = &game_engines[gn];
  else g = NULL;
    seladdr = off + marker;
    TMoveCursor(1, 1);
    if (g) printf ("%-64s\n\n",g->name.c_str());
    printf ("        ");
    for (int i = 0; i < 8; i ++ ) {
      printf ("Ch %d  ",i+1);
    }
    printf("\n");
    if (g) {
    for (int wc = 0; wc < g->watchers.size(); wc++) {
      Watcher* w = &g->watchers[wc];
      TResetColor();
      printf("%6s: ",w->id.c_str());
      uint16_t val;
      if (w->size == 1) {
        for (int i = 0; i < 8; i ++) {
          if (!mutedchannel[i]) TSetColor(chancolors[i]);
          val = spc->apu.m.ram.ram[w->base + (i*w->skip)];
          printf ("  %02X  ",val);
          TResetColor();
        }
        if (w->id == "tick") {
          TResetColor();
          printf("\n");
          printf("        ");

          for (int i = 0; i < 8; i ++) {
            if (!mutedchannel[i]) TSetColor(chancolors[i]);
            val = spc->apu.m.ram.ram[w->base + (i*w->skip)];
            if (val < 0x8) printf ("      ");
            else if (val < 0x10) printf ("-     ");
            else if (val < 0x18) printf ("--    ");
            else if (val < 0x20) printf ("---   ");
            else if (val < 0x30) printf ("----  ");
            else printf ("----- ");
            TResetColor();
          }
        }
        else if (w->id == "note") {
          TResetColor();
          printf("\n");
          printf("        ");

          for (int i = 0; i < 8; i ++) {
            if (!mutedchannel[i]) TSetColor(chancolors[i]);
            val = spc->apu.m.ram.ram[w->base + (i*w->skip)];
            int actnote = val + w->props["transpose"];
            int nn = actnote % 12;
            int oct = actnote / 12;
            if (actnote < 0 || val == 0xFF) {
              printf ("----- ");
            }
            else printf ("%s%-2d  ",notes[nn],oct);
            TResetColor();
          }
        }
      }
      else if (w->size == 2) {
        for (int i = 0; i < 8; i ++) {
          if (!mutedchannel[i]) TSetColor(chancolors[i]);
          if (w->base_hi != 0xFFFFFFFF) {
            val = spc->apu.m.ram.ram[w->base_hi + (i*w->skip)] * 0x100;
            val += spc->apu.m.ram.ram[w->base + (i*w->skip)];

          }
          else {
            val = spc->apu.m.ram.ram[w->base + 1 + (i*w->skip)] * 0x100 + spc->apu.m.ram.ram[w->base + (i*w->skip)];
          }
          if (w->id == "addr") channels.addr[i] = val;
          printf ("%04X  ",val);
          TResetColor();
        }
      }
      TResetColor();
      printf("\n");
    }
    }


      printf ("        ");
      for (int i = 0; i < 8; i ++ ) {
        printf ("      ");
      }
         printf("\n");


    printf("\n");
    TResetColor();
    printf("        ");
      for (int i = 0; i < range; i++)
        printf("%02X ",i);
      printf("\n");
    TResetColor();
    printf("        ");
      for (int i = 0; i < range; i++)
        printf("---");
      printf("\n");

    TResetColor();

    for (int j = 0; j< rows;j ++) {
      printf ("  %04X: ",off+(j * range));
      for (int i = 0; i < range; i++) {
        uint16_t addr = off+i+(j * range);
        uint8_t val = spc->apu.m.ram.ram[addr];
        int isch = -1;
        for (int c = 0; c < 8; c++) {
          if (channels.addr[c] == addr) {
            isch = c;
            break;
          }
        }
        if (addr == seladdr) {
          TSetColor(47);
          TSetColor(30);
          printf("%02X",val);
          TResetColor();
          printf(" ");
        }
        else {
          if (isch >= 0)             TSetColor(chancolors[isch]);
          printf("%02X ",val);
          if (isch >= 0)             TSetColor(0);
        }
      }
      printf("\n");
    }
    if (kbhit()) {
      int read = fgetc(stdin);
      //        TMoveCursor(1, 20);
      if (read == 'q') {
        TClearScreen();
        TMoveCursor(1, 1);
        printf("Exiting..\n");
        break;
      }
      else if (read == 'g') {
        spc->set_tempo(0.0);
        TClearScreen();
        TMoveCursor(1, 1);
        printf("Goto Address: ");
        int val;
        scanf("%04X",&val);
        off = val / range * range;
        if (off > (0x10000 - (rows * range))) off = 0x10000 - (rows * range);
        marker = val - off;
        if (marker >= (rows * range)) marker = (rows * range - 1);
        spc->set_tempo(emurate);
      }
      else if (read == 'e') {
        spc->set_tempo(0.0);
        TClearScreen();
        TMoveCursor(1, 1);
        printf("New value for %04X: ",seladdr);
        int val;
        scanf("%02X",&val);
        spc->apu.m.ram.ram[seladdr] = val;
        spc->set_tempo(emurate);
      }
      else if (read == 't') {
        spc->set_tempo(0.0);
        TClearScreen();
        TMoveCursor(1, 1);
        printf("Set Rate: ");
        double val;
        scanf("%lf",&val);
        emurate = val;
        if (emurate < 0.0) emurate = 0.1;
        if (emurate > 16.0) emurate = 16.0;
        spc->set_tempo(emurate);
      }
      else if (read == 'r') {
        spc->seek(0);
      }
      else if (read == 'w') {
        off -= skip;
        if (off < 0) off = 0;
      }
      else if (read == 's') {
        off += skip;
        if (off > (0x10000 - (rows * range))) off = (0x10000 - (rows * range));
      }
      else if (read == 'i') {
        marker -= range;
        if (marker < 0) {
          marker += range;
          off -= range;
          if (off < 0) {
            off += range;

          }

        }
      }
      else if (read == 'j') {
        marker -= 1;
        if (marker < 0) {
          marker += range;
          off -= range;
          if (off < 0) {
            off += range;

          }

        }
      }

      else if (read == 'k') {
        marker += range;
        if (marker >= (rows * range)) {
          marker -= range; off += range;
          if (off > (0x10000 - (rows * range))) {
            off -= range;
          }
        }
      }
      else if (read == 'l') {
        marker += 1;
        if (marker >= (rows * range)) {
          marker -= range;
          off += range;
          if (off > (0x10000 - (rows * range))) {
            off -= range;
          }
        }
      }
      else if (read == '!') {
        off = channels.addr[0] / range * range;
      }
      else if (read == '@') {
        off = channels.addr[1] / range * range;
      }
      else if (read == '#') {
        off = channels.addr[2] / range * range;
      }
      else if (read == '$') {
        off = channels.addr[3] / range * range;
      }
      else if (read == '%') {
        off = channels.addr[4] / range * range;
      }
      else if (read == '^') {
        off = channels.addr[5] / range * range;
      }
      else if (read == '&') {
        off = channels.addr[6] / range * range;
      }
      else if (read == '*') {
        off = channels.addr[7] / range * range;
      }
      else if (read == '`') {
        gn = gn + 1;
        if (gn >= game_engines.size()) gn = -1;
      }
      else if (read == '~') {
        load_game_engines();
        TClearScreen();
      }
    else  if (read == '1') ToggleMute(0);
    else if (read == '2') ToggleMute(1);
    else if (read == '3') ToggleMute(2);
    else if (read == '4') ToggleMute(3);
    else if (read == '5') ToggleMute(4);
    else if (read == '6') ToggleMute(5);
    else if (read == '7') ToggleMute(6);
    else if (read == '8') ToggleMute(7);
    else if (read == 'z') if (!(soloing && solochannel==0)) { ToggleSolo(0); } else { ResetVol(); }
    else if (read == 'x') if (!(soloing && solochannel==1)) { ToggleSolo(1); } else { ResetVol(); }
    else if (read == 'c') if (!(soloing && solochannel==2)) { ToggleSolo(2); } else { ResetVol(); }
    else if (read == 'v') if (!(soloing && solochannel==3)) { ToggleSolo(3); } else { ResetVol(); }
    else if (read == 'b')  if (!(soloing && solochannel==4)) { ToggleSolo(4); } else { ResetVol(); }
    else if (read == 'n')  if (!(soloing && solochannel==5)) { ToggleSolo(5); } else { ResetVol(); }
    else if (read == 'm')  if (!(soloing && solochannel==6)) { ToggleSolo(6); } else { ResetVol(); }
    else if (read == ',')  if (!(soloing && solochannel==7)) { ToggleSolo(7); } else { ResetVol(); }
    else if (read == '.') ResetVol();


    }

  }
    err = Pa_StopStream( stream );
    if( err != paNoError ) goto error;

    err = Pa_CloseStream( stream );
    if( err != paNoError ) goto error;

    err = Pa_Terminate();
    if( err != paNoError )
       printf(  "PortAudio error: %s\n", Pa_GetErrorText( err ) );


   gme_delete(data.emu);
    return 0;

    error:
    printf(  "PortAudio error: %s\n", Pa_GetErrorText( err ) );
    return 1;
}
*/

