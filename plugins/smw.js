function pluginRegister() {
    plugin.name = 'Super Mario World';
    plugin.watchers = [
        { id: 'addr', base: 0x30, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0x2B1, size: 0x01, skip: 0x02, prop: { transpose: 0 }},
        { id: 'inst', base: 0xC1, size: 0x01, skip: 0x02 },  
        { id: 'tick', base: 0x70, size: 0x01, skip: 0x02 }        
    ];
}