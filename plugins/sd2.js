function pluginRegister() {
    plugin.name = 'Secret of Mana';
    plugin.watchers = [
        { id: 'addr', base: 0x000C, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0xFD41, size: 0x01, skip: 0x02, prop: { transpose: -2 }},
        { id: 'tick', base: 0x40, size: 0x01, skip: 0x02 },        
        { id: 'inst', base: 0x0051, size: 0x01, skip: 0x02 },        
        { id: 'octv', base: 0x41, size: 0x01, skip: 0x02 }        
    ];
}