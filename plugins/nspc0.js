function pluginRegister() {
    plugin.name = 'N-SPC 0';
    plugin.watchers = [
        { id: 'addr', base: 0x30, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0x361, size: 0x01, skip: 0x02, prop: { transpose: 0 }},
        { id: 'tick', base: 0x70, size: 0x01, skip: 0x02 },  
        { id: 'len', base: 0x200, size: 0x01, skip: 0x02 }        
    ];
}