function pluginRegister() {
    plugin.name = 'Final Fantasy VI';
    plugin.watchers = [
        { id: 'addr', base: 0x0002, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0xF761, size: 0x01, skip: 0x02, prop: { transpose: -2 }},
        { id: 'tick', base: 0x25, size: 0x01, skip: 0x02 },        
        { id: 'inst', base: 0xF601, size: 0x01, skip: 0x02 },        
        { id: 'octv', base: 0xF600, size: 0x01, skip: 0x02 }        
    ];
}