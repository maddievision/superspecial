function pluginRegister() {
    plugin.name = 'Rock n Roll Racing';
    plugin.watchers = [
        { id: 'addr', base: 0x30, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0x90, size: 0x01, skip: 0x02, prop: { transpose: 0 }},
        { id: 'tick', base: 0x80, size: 0x01, skip: 0x02 }        
    ];
}