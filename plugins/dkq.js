function pluginRegister() {
    plugin.name = 'Donkey Kong Country 2';
    plugin.watchers = [
        { id: 'addr', base: 0x44, base_hi: 0x54, size: 0x02, skip: 0x01 },
        { id: 'note', base: 0x2B1, size: 0x01, skip: 0x02, prop: { transpose: 0 }},
        { id: 'tick', base: 0x34, size: 0x01, skip: 0x01 },  
        { id: 'freq', base: 0x84, base_hi: 0x74, size: 0x02, skip: 0x01 }        
    ];
}