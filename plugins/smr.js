function pluginRegister() {
    plugin.name = 'Super Mario RPG';
    plugin.watchers = [
        { id: 'addr', base: 0x1BFC, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0x1885, size: 0x01, skip: 0x02, prop: { transpose: 2 }},
        { id: 'tick', base: 0xB2, size: 0x01, skip: 0x01 }        
    ];
    plugin.watchers2 = [
        { id: 'addr', base: 0x1C0C, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0x1895, size: 0x01, skip: 0x02, prop: { transpose: 2 }},
        { id: 'tick', base: 0xBA, size: 0x01, skip: 0x01 },        
        { id: 'octv', base: 0x1844, size: 0x01, skip: 0x01 }        
    ];

}