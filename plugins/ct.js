function pluginRegister() {
    plugin.name = 'Chrono Trigger';
    plugin.watchers = [
        { id: 'addr', base: 0x0002, size: 0x02, skip: 0x02 },
        { id: 'note', base: 0xF3A1, size: 0x01, skip: 0x02, prop: { transpose: -2 }},
        { id: 'tick', base: 0x25, size: 0x01, skip: 0x02 }        
    ];
}