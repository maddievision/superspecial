#include "superspecialmainwindow.h"
#include "ramviewwindow.h"
#include "superspecialcore.h"
SuperSpecialMainWindow::SuperSpecialMainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QSettings settings;
    this->core = new SuperSpecialCore();
    this->lastPath = settings.value("lastPath",QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + QDir::separator() + "*").toString();
    this->setWindowTitle("SuperSpecial");
    this->resize(800,600);
    this->setupMenu();
    this->setupTabs();
    this->show();
}
void SuperSpecialMainWindow::setupTabs() {
    this->tabWidget = new QTabWidget();
    this->tabWidget->addTab(new RamViewWindow(),"RAM");
    this->setCentralWidget(this->tabWidget);
}

void SuperSpecialMainWindow::openFile(QString fname) {
//    this->visualWindow->stopVisuals();
    this->stopPlayback();
    QSettings settings;
    this->filename = fname;
    QFileInfo fi(fname);
    this->core->openFile(fname);
//    this->nutCore->openFile(fname.toUtf8());
//    this->visualWindow->resetAudioUpdater();
    this->setWindowTitle(QString("SuperSpecial - %1").arg(fi.baseName()));
}

void SuperSpecialMainWindow::playFromStart() {
    this->core->playFromStart();
//    this->visualWindow->resetAudioUpdater();
//    this->nutCore->playFromStart();
//    this->visualWindow->startVisuals();
}

void SuperSpecialMainWindow::resumePlayback() {
    this->core->resume();
//    this->nutCore->resumePlayback();
//    this->visualWindow->startVisuals();
}

void SuperSpecialMainWindow::stopPlayback() {
    this->core->stop();
//    this->visualWindow->stopVisuals();
//    this->nutCore->stopPlayback();
}

void SuperSpecialMainWindow::fileOpen() {
    QSettings settings;
    QString fname = QFileDialog::getOpenFileName(this,"Open SPC File",this->lastPath,"SPC700 Save States (*.spc);*.spc");
    if (fname.isNull()) return;
    QFileInfo fi(fname);
//    m_box(this->filename);
    this->lastPath = fi.path() + QDir::separator() + "*";
    settings.setValue("lastPath",this->lastPath);
    settings.sync();
    this->openFile(fname);
}

void SuperSpecialMainWindow::setupMenu() {
    this->fileMenu = this->menuBar()->addMenu("&File");
    this->fileOpenAction = this->fileMenu->addAction("&Open");
    this->fileOpenAction->setShortcuts(QKeySequence::Open);
    connect(this->fileOpenAction,SIGNAL(triggered()),this,SLOT(fileOpen()));

    this->optionsMenu = this->menuBar()->addMenu("&Options");

    this->controlMenu = this->menuBar()->addMenu("&Control");
    this->controlPlayFromStartAction = this->controlMenu->addAction("&Play from Start");
    this->controlPlayFromStartAction->setShortcut(Qt::Key_F5);
    connect(this->controlPlayFromStartAction,SIGNAL(triggered()),this,SLOT(playFromStart()));
    this->controlResumePlaybackAction = this->controlMenu->addAction("&Resume Playback");
    this->controlResumePlaybackAction->setShortcut(Qt::Key_F7);
    connect(this->controlResumePlaybackAction,SIGNAL(triggered()),this,SLOT(resumePlayback()));
    this->controlStopAction = this->controlMenu->addAction("&Stop");
    this->controlStopAction->setShortcut(Qt::Key_F8);
    connect(this->controlStopAction,SIGNAL(triggered()),this,SLOT(stopPlayback()));
}
