#include "superspecialmainwindow.h"
#include <QApplication>

int main(int argc, char** argv) {
    QApplication::setOrganizationDomain("nolimitzone.com");
    QApplication::setOrganizationName("No Limit Zone");
    QApplication::setApplicationName("SuperSpecial");
    QApplication a(argc, argv);
    SuperSpecialMainWindow mainWindow;
    return a.exec();
}
